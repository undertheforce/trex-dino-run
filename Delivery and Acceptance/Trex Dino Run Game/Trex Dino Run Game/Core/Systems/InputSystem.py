#-------------------------------------------------------------------------------------------------
# File: InputSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: Input System class is responsible for getting input from pygame events and functions to 
# give input to the user during each update.
#-------------------------------------------------------------------------------------------------
import pygame,pygame_gui
class InputSystem:
    """Input System class is responsible for getting input from pygame events and functions to give input to the user during each update"""
    def __init__(self):
        """Function initializes the input dictionary with all required event checks"""
        self.input_dict = dict()
        #self.ui_dict = dict()

    def set_ui_manager(self,manager):
        """Sets the UI Manager in the game"""
        self.ui_manager = manager

    def is_key_pressed(self,event_key):
        """Function returns true if the key was pressed this frame, else returns false"""
        if event_key not in self.input_dict: #first tracking request
            self.input_dict[event_key] = False
            resolve_all_events()
        return self.input_dict[event_key]

    #def is_ui_button_pressed(self,element_name):
    #    """Function returns true if the user button was pressed this frame, else returns false"""
    #    if element_name not in self.ui_dict: #first tracking request
    #        self.ui_dict[element_name] = False
    #        resolve_all_events()
    #    return self.ui_dict[element_name]

    def resolve_all_events(self):
        """Function resolves all events registered for input"""
        for event in pygame.event.get():
            # check if user quit the game
            if event.type == pygame.QUIT:
                return False
            ## check for ui event
            #if event.type == pygame.USEREVENT:
            #    if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            #        for element_name in self.ui_dict:
            #            if event.ui_element == element_name:
            #                self.ui_dict[element_name] = True
            # check for key events
            if event.type == pygame.KEYDOWN:
                for event_key in self.input_dict:
                    if event.key == event_key:
                        self.input_dict[event_key] = True
            if event.type == pygame.KEYUP:
                for event_key in self.input_dict:
                    if event.key == event_key:
                        self.input_dict[event_key] = False
            self.ui_manager.process_events(event)
        return True

    #def clear_ui_events(self):
    #    """Function clears ui events to ensure multiple calls aren't made"""
    #    for element_name in self.ui_dict:
    #        self.ui_dict[element_name] = False

