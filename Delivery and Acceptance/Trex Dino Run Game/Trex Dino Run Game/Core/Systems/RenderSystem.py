#-------------------------------------------------------------------------------------------------
# File: RenderSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: Render System class is responsible for keeping track of the main pygame display and
# instantiate and track all renderable game surfaces
#-------------------------------------------------------------------------------------------------
import pygame
#from Core.Objects.GameObject import GameObject
class RenderSystem():
    """Render System class is responsible for keeping track of the main pygame display and instantiate and track all renderable game surfaces"""
    def __init__(self,window_size,window_default_color, caption):
        """Function to initialize render system and create a pygame window of the given size and color"""
        pygame.display.set_caption(caption)
        self.game_window = pygame.display.set_mode(window_size)
        self.window_size = window_size;
        self.default_color = window_default_color;
        self.drawables = dict()

    def register_drawable(self,game_object):
        """Registers a surface with the system to draw on screen"""
        if game_object.instance_id in self.drawables: #don't register already registered object
            return
        self.drawables[game_object.instance_id] = game_object

    def set_ui_manager(self,manager):
        """Sets the UI Manager in the game"""
        self.ui_manager = manager

    def update_display(self):
        """Function clears the current screen and updates all components on the screen"""
        self.game_window.fill(self.default_color)
        dirty_rects = []
        for instance_id in self.drawables:
            if self.drawables[instance_id].is_dirty:
                self.game_window.blit(self.drawables[instance_id].graphic, self.drawables[instance_id].rect)
                dirty_rects.append(self.drawables[instance_id].rect)
        self.ui_manager.draw_ui(self.game_window)
        pygame.display.update()
        #if len(dirty_rects) > 0:
        #    pygame.display.update(dirty_rects)



