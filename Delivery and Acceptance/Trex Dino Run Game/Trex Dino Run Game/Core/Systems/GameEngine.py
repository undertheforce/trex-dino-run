#-------------------------------------------------------------------------------------------------
# File: GameEngine.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: This class controls the game loop and initializes all game components.
#-------------------------------------------------------------------------------------------------
import pygame
from Core.Systems.InputSystem import InputSystem
from Core.Systems.RenderSystem import RenderSystem

class GameEngine:
    """This class controls the game loop and initializes all game components."""
    def __init__(self,game_manager,window_size,default_color,caption):
        """This initializes pygame and all the respective systems of the game"""
        pygame.init()
        #initialize all other systems
        self.input_system = InputSystem()
        self.render_system = RenderSystem(window_size,default_color,caption)
        self.game_clock = pygame.time.Clock()
        self.game_manager = game_manager;
        if self.game_manager == None:
            raise Exception("No Game Manager")
        self.game_manager.initialize()
        self.input_system.set_ui_manager(self.game_manager.ui_manager)
        self.render_system.set_ui_manager(self.game_manager.ui_manager)

    def game_loop(self):
        """This function loops through and provides updates to each system"""
        while True:
            time_delta = self.game_clock.tick(60)/1000.0
            if not self.input_system.resolve_all_events():
                return
            
            self.game_manager.update(time_delta)
            self.render_system.update_display()
            #self.input_system.clear_ui_events()
            