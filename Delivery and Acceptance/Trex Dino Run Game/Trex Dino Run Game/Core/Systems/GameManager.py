#-------------------------------------------------------------------------------------------------
# File: GameManager.py
#
# Author: Dakshvir Singh Rehill
# Date: 11/02/2021
#
# Summary: Class created to allow for managers to be able to get updates from the engine
#-------------------------------------------------------------------------------------------------
from abc import ABC, abstractmethod
import pygame_gui

class GameManager(ABC):
    """Class created to allow for managers to be able to get updates from the engine"""
    @abstractmethod
    def initialize(self):
        pass

    @abstractmethod
    def update(self,time_delta):
        pass

    def set_ui_manager(self, window_size, theme_path, live_updates):
        self.ui_manager = pygame_gui.UIManager(window_size,theme_path,live_updates)


