#-------------------------------------------------------------------------------------------------
# File: GameObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: Each object in the game will be a gameobject to be able to move collide or render it
#-------------------------------------------------------------------------------------------------

class GameObject():
    """Each object in the game will be a gameobject to be able to move collide or render it"""
    def __init__(self,unique_id):
        """This method will instantiate a default game object with a unique id provided. Use helper from GameObjectManager to get an id"""
        self.instance_id = unique_id
        self.is_dirty = False

    def create_drawable_object(self,rect,graphic):
        """Initializes the object with the drawable components i.e. the rect and the graphic object"""
        self.graphic = graphic
        self.rect = rect
        self.is_dirty = True