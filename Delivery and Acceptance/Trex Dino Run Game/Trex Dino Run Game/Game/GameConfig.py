#-------------------------------------------------------------------------------------------------
# File: GameConfig.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: File created to have a global configuration for the game to ensure data driven practices
#-------------------------------------------------------------------------------------------------
WINDOW_NAME = "Trex Dino AI Run"
WINDOW_SIZE = 1280,720
WINDOW_DEFAULT_COLOR = 0,0,0


UI_THEME_PATH = "Game/Assets/default_ui_theme.json"
UI_LIVE_UPDATE = True


IMG_COMPANY_LOGO = "Game/Assets/company_logo.png"
SIZE_COMPANY_LOGO = 200,200
IMG_GAME_LOGO = "Game/Assets/game_logo.png"
SIZE_GAME_LOGO = 630,100
SIZE_MAIN_MENU_BUTTON = 450,100
MAIN_MENU_GAP = 20
COMPANY_LOGO_SPACING = 50,50

TXT_REG_BTN = "Regular Mode"
TXT_VIEW_BTN = "View Mode"
TXT_GHOST_BTN = "Ghost Mode"
TXT_FRSH_BTN = "Fresh Mode"