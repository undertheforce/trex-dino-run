#-------------------------------------------------------------------------------------------------
# File: DinoRunManager.py
#
# Author: Dakshvir Singh Rehill
# Date: 11/02/2021
#
# Summary: Manager responsible for all workings of the dino game including the bot functionality
#-------------------------------------------------------------------------------------------------
from Core.Systems.GameManager import GameManager
import Game.GameConfig as game_config
import pygame,pygame_gui

class DinoRunManager(GameManager):
    """Manager responsible for all workings of the dino game including the bot functionality"""
    def __init__(self):
        pass

    def initialize(self):
        """Initialize Requirements of the Game"""
        self.set_ui_manager(game_config.WINDOW_SIZE,game_config.UI_THEME_PATH,game_config.UI_LIVE_UPDATE)
        self.create_menu_panels()
        self.game_state = 0
        self.populate_main_menu()
        

    def update(self,time_delta):
        """Update all game objects and move accordingly"""
        self.ui_manager.update(time_delta)
        #use game states to check ui updates
        if self.game_state == 0:
            self.check_main_menu_buttons()
        
    def check_main_menu_buttons(self):
        """Function to check main menu button presses"""
        if self.regular_button.check_pressed():
            #launch regular game
            print("Regular Button Pressed")
            self.game_state = 1
        if self.view_button.check_pressed():
            #launch view mode
            print("View Button Pressed")
            self.game_state = 1
        if self.ghost_button.check_pressed():
            #launch ghost mode
            print("Ghost Button Pressed")
            self.game_state = 1
        if self.fresh_button.check_pressed():
            #launch fresh train mode
            print("Fresh Button Pressed")
            self.game_state = 1

    def create_menu_panels(self):
        """Function to create menu panels that will be used in the game"""
        self.main_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                           ,10,self.ui_manager)
        self.hud_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                          , 5, self.ui_manager)
        self.game_over_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                                , 8, self.ui_manager)
        self.main_menu_panel.disable()
        self.game_over_menu_panel.disable()
        self.hud_menu_panel.disable()

    def populate_main_menu(self):
        """Function to create all interactive elements in the main menu panel"""
        self.main_menu_panel.enable()
        pygame_gui.elements.UIImage(pygame.Rect((325,30),game_config.SIZE_GAME_LOGO)
                                    ,pygame.image.load(game_config.IMG_GAME_LOGO),
                                    self.ui_manager,self.main_menu_panel.get_container())
        button_container = pygame_gui.elements.UIPanel(pygame.Rect((415,170),
                                                (game_config.SIZE_MAIN_MENU_BUTTON[0],
                                                 game_config.SIZE_MAIN_MENU_BUTTON[1] * 4 
                                                 + game_config.MAIN_MENU_GAP * 5)),
                                    2,self.ui_manager,container=self.main_menu_panel.get_container())
        self.regular_button = pygame_gui.elements.UIButton(pygame.Rect((0,game_config.MAIN_MENU_GAP)
                                                                       ,game_config.SIZE_MAIN_MENU_BUTTON),
                                                           game_config.TXT_REG_BTN,self.ui_manager,
                                                           button_container.get_container())
        spacing = game_config.MAIN_MENU_GAP * 2 + game_config.SIZE_MAIN_MENU_BUTTON[1]
        self.view_button = pygame_gui.elements.UIButton(pygame.Rect((0,spacing),game_config.SIZE_MAIN_MENU_BUTTON),
                                                        game_config.TXT_VIEW_BTN,self.ui_manager,
                                                        button_container.get_container())
        spacing = spacing + game_config.MAIN_MENU_GAP + game_config.SIZE_MAIN_MENU_BUTTON[1]
        self.ghost_button = pygame_gui.elements.UIButton(pygame.Rect((0,spacing), game_config.SIZE_MAIN_MENU_BUTTON),
                                                         game_config.TXT_GHOST_BTN,self.ui_manager,
                                                         button_container.get_container())
        spacing = spacing + game_config.MAIN_MENU_GAP + game_config.SIZE_MAIN_MENU_BUTTON[1]
        self.fresh_button = pygame_gui.elements.UIButton(pygame.Rect((0,spacing),game_config.SIZE_MAIN_MENU_BUTTON),
                                                         game_config.TXT_FRSH_BTN,self.ui_manager,
                                                         button_container.get_container())
        logo_location = (game_config.WINDOW_SIZE[0] - game_config.COMPANY_LOGO_SPACING[0] - game_config.SIZE_COMPANY_LOGO[0],
                         game_config.WINDOW_SIZE[1] - game_config.COMPANY_LOGO_SPACING[1] - game_config.SIZE_COMPANY_LOGO[1])
        pygame_gui.elements.UIImage(pygame.Rect(logo_location,game_config.SIZE_COMPANY_LOGO),
                                    pygame.image.load(game_config.IMG_COMPANY_LOGO),
                                    self.ui_manager, self.main_menu_panel.get_container())



