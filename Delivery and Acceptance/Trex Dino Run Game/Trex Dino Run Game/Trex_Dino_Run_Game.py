#-------------------------------------------------------------------------------------------------
# File: Trex_Dino_Run_Game.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: This is the entry point of the game that will be responsible for initial launch and final cleanup of the game
#-------------------------------------------------------------------------------------------------
from Core.Systems.GameEngine import GameEngine
import Game.GameConfig as game_config
from Game.DinoRunManager import DinoRunManager

class TrexDinoGame():
    """Wrapper class to allow for running the game in an external environment"""
    def __init__(self):
        self.game_manager = DinoRunManager()
        self.game_engine = GameEngine(self.game_manager,game_config.WINDOW_SIZE,game_config.WINDOW_DEFAULT_COLOR,game_config.WINDOW_NAME)
    
    def run_trex_dino_game(self):
        self.game_engine.game_loop()

# Testing code
dino_game = TrexDinoGame()
dino_game.run_trex_dino_game()